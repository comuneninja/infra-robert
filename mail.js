/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// This script will send an image as an email attachment to the
// user himself. The receiving part of this is in read.coffee

// Install EmailJS with `npm install emailjs`
const email = require("emailjs");
const events = require('events');

// You need a config file with your email settings
const fs = require("fs");
const config = JSON.parse(fs.readFileSync(`${process.cwd()}/config.json`, "utf-8"));
const notifier = require('mail-notifier');

var eventEmitter = new events.EventEmitter();

const imap = {
 user: "robert@reamp.com.br",
 password: "U3[p+:hV3;ZdSpN6",
 host: "imap.gmail.com",
 port: 993, // imap port 
 tls: true,// use secure connection 
 tlsOptions: { rejectUnauthorized: false }
};

const server = email.server.connect({
    user: config.username,
    password: config.password,
    host: config.smtp.host,
    ssl: config.smtp.ssl
});

module.exports = {
    send: function(emailTo, subject, message, attachments) {
        var message = email.message.create({ 
            text: message,
            from: `${config.name} <${config.email}>`,
            to: `${emailTo.name} <${emailTo.email}>`,
            subject: subject
        });
        
        for(a in attachments) {
            message.attach(a.fileName, a.fileType, a.fileLocation);
        }
        
        server.send(message, function(err, message) {
            if (err) { return console.error(err); }
            return console.log(`Message sent with id ${message['header']['message-id']}`);
        });
    },
    inbox: eventEmitter
}

notifier(imap)
.on('end', () => n.start()) // session closed 
.on('mail', mail => eventEmitter.emit('mail', mail.from[0].address, mail.subject))
.start();