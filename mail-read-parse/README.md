Some examples of sending and receiving emails with Node.js.

This uses the following libraries:

* [emailjs](https://github.com/eleith/emailjs) for sending email
* [node-imap](https://github.com/mscdex/node-imap) for receiving email
* [mailparser](https://github.com/andris9/mailparser) for parsing received emails

## Running

Copy `config.json.example` to `config.json` and enter your email account details.

Run `coffee send.coffee` to send yourself an email with an attachment.

Run `coffee read.coffee` to receive the sent email and write the attachment back to disk with a new name.
