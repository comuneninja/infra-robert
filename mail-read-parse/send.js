/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// This script will send an image as an email attachment to the
// user himself. The receiving part of this is in read.coffee

// Install EmailJS with `npm install emailjs`
const email = require("emailjs");

// You need a config file with your email settings
const fs = require("fs");
const config = JSON.parse(fs.readFileSync(`${process.cwd()}/config.json`, "utf-8"));

const server = email.server.connect({
    user: config.username,
    password: config.password,
    host: config.smtp.host,
    ssl: config.smtp.ssl
});

const message = email.message.create({ 
    text: "This is test",
    from: `${config.name} <${config.email}>`,
    to: `${config.name} <${config.email}>`,
    subject: "Testing Node.js email capabilities"
});

message.attach("reading.png", "image/png", "reading-image.png");

server.send(message, function(err, message) {
    if (err) { return console.error(err); }
    return console.log(`Message sent with id ${message['header']['message-id']}`);
});

module.exports = {
    sendMail: function(emailTo, message) {
        
    }
}