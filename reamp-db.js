var _ = require('underscore')
var mysql = require('mysql');

var getDateMysql =  function () {
    var date = new Date();
    date = date.getUTCFullYear() + '-' +
        ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + date.getUTCDate()).slice(-2) + ' ' +
        ('00' + date.getUTCHours()).slice(-2) + ':' +
        ('00' + date.getUTCMinutes()).slice(-2) + ':' +
        ('00' + date.getUTCSeconds()).slice(-2);
    return date;
};

// Mysql
var con = mysql.createConnection({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DB
});

module.exports = {

    verifyFormat: function(width, height) {
        return new Promise(function (resolve, reject) {
            if (isNaN(width) || isNaN(height)) {
                reject("Formato não numérico.");
            } else {
                resolve(width, height);
            }
        });
    },

    registerFormat: function (width, height) {
        var msgOk = "Formato cadastrado!";
        var msgError = "Erro cadastrando formato " + width + " x "+ height;

        var msg = this.verifyFormat(width, height)
            .then(() => {
                return this.isNewFormat(width, height);
            })
            .then(() => {
                return this.insertFormat(width, height);
            })
            .then(() => {
                console.log(msgOk);
                return msgOk;
            }).catch(err => {
                console.log(msgError + ". " + err);
                return msgError + ". " + err;
            });

        return Promise.resolve(msg);
    },

    userIpPerDay: function (account, user, dayString, zoneId) {
        return new Promise(function (resolve, reject) {
            // relatorios de user_ip por dia
            var startDate = moment(dayString, 'DD/MM/YYYY').startOf('day').toDate();
            //var startDate = new Date(dia.getFullYear(), dia.getMonth(), dia.getDate(), 0, 0, 0);
            var endDate = moment(dayString, 'DD/MM/YYYY').endOf('day').toDate();

            var startDateStr = moment(startDate).format("YYYY-MM-DD HH:mm:ss");
            var endDateStr = moment(endDate).format("YYYY-MM-DD HH:mm:ss");

            var insertQuery = "INSERT INTO `report_jobs` (`start_date`, `end_date`, `report_type`, `file_name`, `account_id`, `user_id`, `params`, `status`,`created_at`,`updated_at`) VALUES ('" + startDateStr + "', '" + endDateStr + "', 'report', '" + ("report-userip-" + account + "-" + randomName(5)) + "', " + account + ", " + user + ", " +
                "'select date_format(from_unixtime(il.created_at), \'yyyy-MM-dd\') as day, il.user_ip as ip from interaction_logs il where il.created_at between " + moment(startDate).add(3, 'hours').unix() + " and " + moment(endDate).add(3, 'hours').unix() + " and il.zone_id = " + zoneId + " and event_name = \'click\''" +
                ", 'waiting', '2017-09-19 15:31:00', '2017-09-19 15:31:15');";

            con.query(insertQuery, function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });

        return insertQuery;
    },

    insertFormat: function (width, height) {
        return new Promise(function (resolve, reject) {

            console.log("Cadastrando formato " + width + "x" + height);

            var now = getDateMysql();
            var sql = "insert into template_formats (`name`, `iab`, `custom`, `width`, `height` , `comments` , `created_at`, `updated_at`) values ('" + width + "x" + height + " Custom', 0, null, " + width + ", " + height + ", null, '" + now + "', '" + now + "')";

            con.query(sql, function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    isNewFormat: function (width, height) {
        return new Promise(function (resolve, reject) {
            var sql = "select * from template_formats where width = " + width + " and height = " + height;
            con.query(sql, function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    if (result.length > 0) {
                        reject("Formato já existe.");
                    } else {
                        resolve();
                    }
                }
            });
        });
    },

    getCompletedAnswers: function () {
        // query robert_pending_answers
        return new Promise(function(resolve, reject) {
            var sql = "select * from robert_pending_answers";
            con.query(sql, function (err, result) {
                if(err) {
                    reject();
                } else {
                    resolve(result);
                }
            });
        });
        
    }

}