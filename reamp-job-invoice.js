var moment = require('moment');
var rjobvariables = require('./reamp-job-variables.js');
var axios = require('axios');

var Sequelize = require('sequelize');

var sequelize = new Sequelize(process.env.MYSQL_DB, process.env.MYSQL_USER, process.env.MYSQL_PASS, {
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    pool: {
        max: 500,
        min: 10,
        idle: 5000,
        acquire: 5000,
        evict: 10000,
        handleDisconnects: true
    },
    retry:{max: 3},
    logging: false,
    dialectOptions: {
                requestTimeout: 0,
    }
});

var rjobbilling = require('./reamp-job-billing.js');

var robertTasks = sequelize.define('robert_tasks', {
    username: { type: Sequelize.STRING, allowNull: true },
    userid: Sequelize.STRING,
    message: { type: Sequelize.STRING, allowNull: true },
    jobstatus: { type: Sequelize.STRING, allowNull: true }, // pending, answered, completed
    jobname: { type: Sequelize.STRING, allowNull: true },
    intent: { type: Sequelize.STRING, allowNull: true },
    params: { type: Sequelize.TEXT, allowNull: true }, // json with all job params from watson
});

module.exports = {
  execute: function(bot, message, startDate, endDate, userId) {
          var reampLoginToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzEyfQ.S5zBc662Hrw2ik6149SnGcd7tCYgS8ElLDAZDzeiqUk';
          var invoiceCreatedAt = moment(new Date()).format("YYYY-MM-DD");
          var invoiceUpdatedAt = moment(new Date()).format("YYYY-MM-DD");

          // var accountWOSpecial = (message.watsonData.context.conta.replace("&amp;", "")).replace(/[^a-zA-Z]\s+/ig, "").toUpperCase();
          // var accountWOSpace = (message.watsonData.context.conta.replace("&amp;", " ")).replace(/\s+/ig," ");

          axios.get('http://nemesis.reamp.com.br/v1/accounts', { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } })
            .then(function (response) {
            var arr = []
            response.data.data.map((item) => {
              arr.push({
                "accountName": (item.attributes.name.replace("&", "")).replace(/[^a-zA-Z]\s+/ig, "").toUpperCase(),
                "accountId": item.id
            })
          });

          var account = arr.filter(function(acc) {
            var accountWOSpecial = (message.watsonData.context.nomeEvento.replace("&amp;", "")).replace(/[^a-zA-Z]\s+/ig, "").toUpperCase();
            return acc.accountName == accountWOSpecial;
          });

          var name = "inv" + invoiceCreatedAt;

          axios.get('http://nemesis.reamp.com.br/v1/accounts/name/' + account[0].accountName, { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } }).then(response => {
            var responseBody = response.data;
            if(responseBody){
              axios.get('http://nemesis.reamp.com.br/v1/accounts/' + account[0].accountId,
              { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } }).then(response => {

              var responseBody = response.data;

              if (responseBody) {
                // console.log(responseBody);
                  var account = responseBody.data.id;
                  var params = {
                      "invoice": {
                          "accounts": [account],
                          "name": name,
                          "start_date": startDate,
                          "end_date": endDate,
                          "created_at": invoiceCreatedAt,
                          "updated_at": invoiceUpdatedAt,
                          "user": userId
                      }
                  };
                    //cria o invoice
                   axios.post('http://nemesis.reamp.com.br/v1/invoices', params,
                     { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } }).then(response => {
                       var responseBody = response.data;
                       var id = responseBody.data[0].id;

                                robertTasks.create({
                                      "username": message.user,
                                      "userid": userId,
                                      "message": "",
                                      "jobstatus": "requested",
                                      "jobname": account[0].accountName,
                                      "intent": "invoice",
                                      "params": id
                                  })

                                bot.reply(message, "Entrarei em contato quando o invoice estiver pronto :)");
                                rjobvariables.execute(message);
                              }).catch(error => {
                                bot.reply(message, "Desculpa, mas não consegui tirar o seu invoice. Avisa pro TI que deu esse erro aqui: " + error.response.data.message);
                                rjobvariables.execute(message);
                                console.error(error.response.data.message);
                              });
                            } 
                        }).catch(error => {
                          rjobvariables.execute(message);
                          console.error(error.response.data.message);
                        });
                }
              }).catch(error => {
                bot.reply(message, "Não encontrei essa conta :/");
                rjobvariables.execute(message);
                console.error(error.response.data.message);
              });
            })
            .catch(function (error) {
              console.log(error);
            });

            //tratar caracter especial replace(/[^a-zA-Z]/g, "")

            //tratar mais de um espaço replace(/\s+/ig," ")
        }

  // }
}

//      {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzEyfQ.S5zBc662Hrw2ik6149SnGcd7tCYgS8ElLDAZDzeiqUk","user_id":1312,"account_id":1227,"campaign_id":100067,"creative_group_id":20338,"import_id":178
