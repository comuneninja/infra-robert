var AdmZip = require('adm-zip');
 
// reading archives 
	var zip = new AdmZip("./my_file.zip");
	var zipEntries = zip.getEntries(); // an array of ZipEntry records 
 
	zipEntries.forEach(function(zipEntry) {
        console.log(zipEntry.toString()); // outputs zip entries information 
        if (zipEntry.entryName == "my_file") {
             console.log(zipEntry.data.toString('utf8')); 
        }
    });
    // outputs the content of some_folder/my_file.txt 
    console.log(zip.readAsText("my_file")); 
    // extracts the specified file to the specified location 
    // zip.extractEntryTo(/*entry name*/"Desktop/my_file", /*target path*/"/suicune", /*maintainEntryPath*/false, /*overwrite*/true);
    // extracts everything 
    zip.extractAllTo(/*target path*/"/home/me/zipcontent/", /*overwrite*/true);
    
    
    // creating archives 
    // var zip = new AdmZip();
    
    // // add file directly 
    // zip.addFile("test.txt", new Buffer("inner content of the file"), "entry comment goes here");
    // // add local file 
    // zip.addLocalFile("./my_image");
    // // get everything as a buffer 
    // var willSendthis = zip.toBuffer();
    // // or write everything to disk 
    // zip.writeZip(/*target file name*/"my_file.zip");
    // 