var rjobcalendar = require('./reamp-job-calendar.js');
var rjobinserirformato = require('./reamp-job-inserirformato.js');
var rjobinvoice = require('./reamp-job-invoice.js');
var rjobvariables = require('./reamp-job-variables.js');
var rjobtratamento = require('./reamp-job-tratamento.js');
var rjoboverlap = require('./reamp-job-overlap.js');
var axios = require('axios');
var querystring = require('querystring');
var moment = require('moment');
// TACAR TODOS JOB UM EM CADA ARQUIVO JS E IMPORTAR TUDO AQUI

//
// INSERIR A INTERPRETACAO DOS DIVERSOS JOBS AQUI E CRIAR UM JS PARA CADA JOB
//

module.exports = {
    interpret_request: function (bot, message, person) {
        var nickName = person.nick_name;
        var userId = person.rmcId;
        console.log("message: ",message.watsonData)

        // Watson configured reply
        if(message.watsonData.output.text == null){
            bot.reply(message, "Precisa da minha ajuda pra mais alguma coisa?");
        } else if(message.watsonData.context.system.dialog_request_counter == 1){
            bot.reply(message, nickName + ", " + message.watsonData.output.text.join('\n').toLowerCase());
        } else {
            bot.reply(message, message.watsonData.output.text.join('\n'));
        }

        //jobs
        if (message.watsonData.context.confirmFormat == true) {
            console.log("eae")
            rjobinserirformato.execute(bot, message);
        }

         if (message.watsonData.context.adserverStatus == true) {
            console.log("vamo ver")
            var loadBalances = ["reamp-adserver-missingno--r", "reamp-adserver-missingno--r-2", "reamp-adserver-missingno--o", "reamp-adserver-missingno--o-2"]

            var cloudwatch = new AWS.CloudWatch();
            var endTime = new Date;
            var startTime = new Date;
            startTime.setMinutes(startTime.getMinutes() - 1);

            // MODELO DE PARAMS 
            var params = {
              EndTime: endTime || 'Wed Dec 31 1969 16:00:00 GMT-0800 (PST)' || 123456789, /* required */
              MetricName: '', /* required */
              Namespace: 'AWS/ELB', /* required */
              Period: 60, /* required */
              StartTime: startTime || 'Wed Dec 31 1969 16:00:00 GMT-0800 (PST)' || 123456789, /* required */
              Dimensions: [
                {
                  Name: 'LoadBalancerName', /* required */
                  Value: '' /* required */
                },
                /* more items */
              ],
              Statistics: [
                'Average', 'Sum'
              ],
              Unit: ''
            };

            var promises = new Array();
            var count = 0;
            var msg = ">>> \n"
            for (i=0; i<loadBalances.length; i++){
                params.Dimensions[0].Value = loadBalances[i];

                params.MetricName = "Latency";
                params.Unit = "Seconds";
                var latency;
                var data1;
                var req = cloudwatch.getMetricStatistics(params).promise();
                promises.push(req);

                params.MetricName = "HTTPCode_Backend_2XX"
                params.Unit = "Count"
                var requests;
                var data2
                req = cloudwatch.getMetricStatistics(params).promise();
                promises.push(req);
            };

            Promise.all(promises).then(function(values){
                for(i=0; i<values.length; i++){
                    if (i%2==0){
                        if (i==0)
                            msg = " \n " + msg + loadBalances[i] + " \n";
                        else
                            msg = " \n " + msg + loadBalances[i/2] + " \n";

                        msg = msg + "Latencia " + values[i].Datapoints[0].Average + " \n";
                        
                    } else {
                        msg = " \n" + msg + "Requests " + values[i].Datapoints[0].Sum + " \n";
                    };      
                };
            bot.reply(message, msg);
            console.log(msg);   
            }); 
        }

        // se for algo que a pessoa ta repitindo e o robert nao souber, posta no suporte ti
    }
}