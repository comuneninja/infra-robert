//limpar variáveis

module.exports = {
	execute: function(message) {
		var m = message.watsonData.context;

		m.confirmado = false;
		m.dataInicio = null;
		m.conta = null;
		m.dia = null;
		m.width = null;
		m.height = null;
		m.dataFim = null;
		m.horaFim = null
		m.dimensions = null;
		m.horaInicio = null;
		m.nomeEvento = null;
		m.confirmEvento = false;
		m.confirmFormat = false;
		m.confirmUniqueUser = false;
		m.confirmCriarCampanha = false;
		m.confirmGerarInvoice = false;
		m.confirmOverlap = false;

		console.log("limpei :DD")
	}
}