var rdb = require('./reamp-db.js');
var rjobvariables = require('./reamp-job-variables.js');
var axios = require('axios');

module.exports = {
	execute: function(bot, message) {
		var reampLoginToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzEyfQ.S5zBc662Hrw2ik6149SnGcd7tCYgS8ElLDAZDzeiqUk';

		var width = message.watsonData.context.width;
		var height = message.watsonData.context.height;
		if (width != null && width.length > 0 &&
			height != null && height.length > 0) {

			var params = {
			  "templates_format": {
				"name": width + "x" + height + " Custom",
				"width": width,
				"height": height,
				"created_at": "2017-05-08",
				"updated_at": "2017-05-08",
				"user": "1312"
			  }
			}

			axios.post('http://nemesis.reamp.com.br/v1/templates_formats', params, { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } }).then(response => {
				if(response.data){
					bot.reply(message, "Formato cadastrado!");
					rjobvariables.execute(message);
				}
			}).catch(error => {
				bot.reply(message, "Ocorreu um erro: " + error.response.data.message);
			  	rjobvariables.execute(message);
			});
		}
	}
}