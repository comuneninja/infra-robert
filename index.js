require('dotenv').load();
var capitalize = require('capitalize');
var job = require('./job_interpreter.js');
var unirest = require('unirest');
var axios = require('axios');
var mkdirp = require('mkdirp');
var shell = require('shelljs');
var request = require('request');
var fs = require("fs");
var crypto = require("crypto");
var moment = require('moment');
var Botkit = require('botkit');
var express = require('express');
var rdb = require('./reamp-db.js');
var rdata = require('./reamp-data.js');
var rauth = require('./reamp-auth.js');
var rjobvariables = require('./reamp-job-variables.js');

var reampLoginToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzE0fQ.ZLjbhqtO1ZwmA7GH2MUeRAI1vp6Km1p3D3xEMNh4MYg';

var _ = require('underscore');

var Sequelize = require('sequelize');

var sequelize = new Sequelize(process.env.MYSQL_DB, process.env.MYSQL_USER, process.env.MYSQL_PASS, {
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    pool: {
        max: 500,
        min: 10,
        idle: 5000,
        acquire: 5000,
        evict: 10000,
        handleDisconnects: true
    },
    retry:{max: 3},
    logging: false,
    dialectOptions: {
                requestTimeout: 0,
    }
});

var robertTasks = sequelize.define('robert_tasks', {
    username: { type: Sequelize.STRING, allowNull: true },
    userid: Sequelize.STRING,
    message: { type: Sequelize.STRING, allowNull: true },
    jobstatus: { type: Sequelize.STRING, allowNull: true }, // pending, answered, completed
    jobname: { type: Sequelize.STRING, allowNull: true },
    intent: { type: Sequelize.STRING, allowNull: true },
    params: { type: Sequelize.TEXT, allowNull: true }, // json with all job params from watson
});

var people = sequelize.define('reamp_people', {
    username: { type: Sequelize.STRING, allowNull: true },
    userid: Sequelize.STRING,
    rmcId: {type: Sequelize.STRING, allowNull: true},
    real_name: { type: Sequelize.STRING, allowNull: true },
    nick_name: { type: Sequelize.STRING, allowNull: true },
    email: { type: Sequelize.STRING, allowNull: true },
    area: { type: Sequelize.STRING, allowNull: true }, // opec, midia e audiencia, tecnologia, bi, administrativo
    cargo: { type: Sequelize.STRING, allowNull: true },
    ramal: { type: Sequelize.STRING, allowNull: true },
    status_cadastro: { type: Sequelize.STRING, allowNull: true }, // esperando cadastro, cadastrada
    params: { type: Sequelize.TEXT, allowNull: true }
});

sequelize.sync().then(function (err) {
    // console.log(err);
    if (err) {
        console.log("err sequelize.sync");
    } else {
        console.log('Item table created successfully');
    }
});

// Botkit
var middleware = require('botkit-middleware-watson')({
    username: process.env.CONVERSATION_USERNAME,
    password: process.env.CONVERSATION_PASSWORD,
    workspace_id: process.env.WORKSPACE_ID,
    version_date: '2016-09-20'
});

// Configure your bot.
var slackController = Botkit.slackbot();
var slackBot = slackController.spawn({
    token: process.env.SLACK_TOKEN
});

slackController.hears(['.*'], ['direct_message', 'direct_mention', 'mention'], function(bot, message) {
    // Within context where you have a message object
    var currentUser; 
    slackBot.api.users.info({user:message.user},function(err,response) {
        if(err) {
            bot.say("ERROR :(");
        }
        else {
            // console.log(response["user"]);
            var slackName = response["user"]["name"];
            var firstName = response["user"]["profile"]["first_name"];
            var realName = response["user"]["profile"]["real_name"];
            var email = response["user"]["profile"]["email"];
            var teamid = response["user"]["profile"]["team"];

            if(!firstName) {
                firstName = realName;
            }
            
            currentUser = capitalize.words(firstName);

            console.log("REAL NAME " + realName);

            people.findAll({ where: {userid: message.user} }).then(function (resultSet) {                
                var nickName = firstName;
                var person = null;
                // if not found in database with this real name, register
                if(!resultSet || resultSet.length == 0) {
                    var person = {
                        username: slackName,
                        userid: message.user,
                        nick_name: firstName,
                        real_name: realName,
                        email: email
                    };
                    people.create(person);
                } else {
                    var person = resultSet[0];                    
                    nickName = resultSet[0].nick_name;
                }

                if(nickName) capitalize.words(nickName);

                middleware.interpret(bot, message, function (err) {
                    if (!err) {

                        // verificar no mapa de conversation ids, qto tempo foi da ultima msg dessa conversa

                        var timeoutRegistry = {
                            bot: bot,
                            message: message,
                            person: person,
                            lastMessage: new Date().getTime()
                        };

                        var timeoutKey = message.watsonData.context.conversation_id;

                        timeoutMap[timeoutKey] = timeoutRegistry;
                        

                        job.interpret_request(bot, message, person);
                    }
                });
            });
        }
    });
});

var timeoutMap = {};

function expireSession(bot, message) {
    message.watsonData.context.confirmado = false;
    message.watsonData.context.width = "";
    message.watsonData.context.height = "";
    message.watsonData.context.dia = "";
    message.watsonData.context.horaInicio = "";
    message.watsonData.context.horaFim = "";
    message.watsonData.context.nomeEvento = "";
    message.watsonData.context.conta = "";
    message.watsonData.context.dataInicio = "";

    bot.reply(message, "Sessão expirou");
}

function checkActive() {
    var timeoutLimit = 1000*60*1;
    var currentDate = new Date().getTime();
    // console.log("checking last messages " + Object.keys(timeoutMap).length);
    // percorrer todos os registros do mapa
    Object.keys(timeoutMap).forEach(function(key) {
        timeoutRegistry = timeoutMap[key];
        if(timeoutRegistry == null) return;
        var diffDate = currentDate - timeoutRegistry.lastMessage;
        if(diffDate > timeoutLimit) {
            // vamo matar a conversation
            var bot = timeoutRegistry.bot;
            var message = timeoutRegistry.message;
            expireSession(bot,message);
            delete timeoutMap[key];
        }
        // console.log(diffDate);
    });

    robertTasks.findAll({ where: {jobstatus: 'requested'} }).then(function (resultSet) {
        resultSet.forEach(function (one) {
            var intent = one.intent;
            var id = one.params;

            if(intent === "invoice" && id != null) {          
                axios.get('http://nemesis.reamp.com.br/v1/invoices/' + id, { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } })
                .then(function (response) {
                    var responseBody = response.data;
                    var file = responseBody.data.attributes.file;
                    if (file != undefined && file != null) {
                            // var name = responseBody.data.attributes.name;

                            one.updateAttributes({
                                jobstatus: 'done',
                                message: 'Seu invoice foi processado: ' + file
                            })
                    }
                }).catch(error => {
                    console.error(error.response.data.message)
                });
            }
        })}).catch(error => {
        console.error(error.response.data.message)
    });


    robertTasks.findAll({ where: {jobstatus: 'done'} }).then(function (resultSet) {
        resultSet.forEach(function (one) {
                slackBot.startConversation({ user: one.username}, function (err, dm) {
                    if(!err){
                        dm.say(one.message);

                        one.updateAttributes({
                            jobstatus: 'delivered'
                        })
                    }                        
                })

                if (one.message) {
                    slackBot.startPrivateConversation({ user: one.username }, function (err, dm) {
                        if(dm != undefined){
                            dm.say(one.message);
                        }
                    });
                }
        })
    }).catch(error => {
        console.error(error.response.data.message)
    });

    // process.on('unhandledRejection', (reason, p) => {
    //   console.log(' _______________ Unhandled Rejection at: Promise', p, 'reason:', reason);
    //   // application specific logging, throwing an error, or other logic here
    // });
}

// setInterval(checkActive,6000);

slackBot.startRTM();

slackController.on('file_share', function (bot, file) {

    var currentUser; 
    slackBot.api.users.info({user:file.user},function(err,response) {
        if(err) {
            bot.say("ERROR :(");
        }
        else {
            // console.log(response["user"]);
            var slackName = response["user"]["name"];
            var firstName = response["user"]["profile"]["first_name"];
            var realName = response["user"]["profile"]["real_name"];
            var email = response["user"]["profile"]["email"];
            var teamid = response["user"]["profile"]["team"];

            if(!firstName) {
                firstName = realName;
            }
            
            currentUser = capitalize.words(firstName);

            console.log("REAL NAME " + realName);

            people.findAll({ where: {userid: file.user} }).then(function (resultSet) {
                console.log(resultSet.length);
                
                var nickName = firstName;
                var person = null;
                // if not found in database with this real name, register
                if(!resultSet || resultSet.length == 0) {
                    var person = {
                        username: slackName,
                        userid: file.user,
                        nick_name: firstName,
                        real_name: realName,
                        email: email
                    };
                    people.create(person);
                } else {
                    var person = resultSet[0];                    
                    nickName = resultSet[0].nick_name;
                }

                if(nickName) capitalize.words(nickName);

                middleware.interpret(bot, file, function (err) {
                    if (!err) {
                        job.interpret_request(bot, file, person, slackController);
                    }
                });
            });
        }
    });
    //decompress zip and
    console.log("file shared");

    var base64ZipFile = '';

    // console.log(JSON.stringify(file));

    var todayString = moment(new Date()).format("YYYY-MM-DD");

    var fileLocation = './temp/' + file.file.user + "/" + todayString + "/";

    shell.mkdir('-p', fileLocation);
    mkdirp(fileLocation, function (err) {
        if (err) console.error(err)
        else console.log('pow!')
    });

    var filePath = fileLocation + file.file.id + "-" + file.file.name;
    console.log(filePath)

    var ws = fs.createWriteStream(filePath);
    ws.on('error', function (err) { console.log(err); });

    /*
    request({
        url: file.file.url_private,
        method: "",
        headers: { 'Authorization': 'Bearer ' + slackBot.config.token },
    }, function (err, res, body) {

        base64ZipFile = base64_encode(filePath);
        console.log('converted to base64');

        var importCreatedAt = moment(new Date()).format("YYYY-MM-DD");
        var importUpdatedAt = moment(new Date()).format("YYYY-MM-DD");

        var fileBase64 = "data:application/zip;base64," + base64ZipFile;

        console.log("---------------------------");
        console.log(fileBase64);
        console.log("---------------------------");

        var accountId = "1227";

        var params = {
            "import": {
                "account": accountId,
                "campaign": "100042",
                "file": fileBase64,
                "name": "Import_12-3",
                "archived": false,
                "created_at": importCreatedAt,
                "updated_at": importUpdatedAt,
                "user": "1301"
            }
        };

        axios.post('http://admin-adserver-api.reamp.com.br/v1/imports',
            params,
            { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } }).then(response => {
                console.log("asdouifnaosdiufnao sdifuna osdiufn p");

                var responseBody = response.data;

                if (responseBody) {
                    var importId = responseBody.data.id;

                    var importExecuteUrl = "http://admin-adserver-import-api.reamp.com.br/v1/accounts/" + accountId + "/import/" + importId;

                    console.log("import uploaded, now executing it");
                    console.log(importExecuteUrl);

                    axios.get(importExecuteUrl).then(response => {
                        bot.reply(file, "ok, parece estar tudo ok com o seu zip");
                        bot.reply(file, "to fazendo o seu import aqui e assim que estiver pronto te aviso");
                        bot.reply(file, "a propósito, segue o id do seu import: " + importId);
                        console.log(response.data);
                    }).catch(error => {
                        console.error(error.response.data.message);
                    });

                    // registra a task no role
                    robertTasks.create({
                        "username": "",
                        "userid": "",
                        "message": "Seu import foi recebido!",
                        "jobstatus": "pending",
                        "jobname": "",
                        "intent": "import",
                        "params": importId
                    })
                }

            }).catch(error => {
                console.error(error.response.data.message);
            });
            

        /*
    unirest.post('http://admin-adserver-api.reamp.com.br/v1/imports')
        .headers({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + reampLoginToken })
        .send(params)
        .end(function (response) {
            console.log("server responded");
            console.log(response.body);

            var rjson = JSON.parse(response.body);

            //bot.reply(file, response.body);

            var importId = rjson.data.id;

            console.log("import id from server: " + importId);

            // se nao deu ruim, enviar para 
            // http://admin-adserver-import-api.reamp.com.br/v1/accounts/:account_id/import/:id

            var importExecuteUrl = "http://admin-adserver-import-api.reamp.com.br/v1/accounts/" + accountId + "/import/" + importId;
        });*/

    //}).pipe(ws);
    

});

slackController.on('rtm_open', function (bot) {
    console.log("on")
});

// Create an Express app
var app = express();
var port = process.env.PORT || 5000;
app.set('port', port);

app.listen(port, function () {
    console.log('Client server listening on port ' + port);
});

app.get('/health', function (req, res) {
    res.send(Date.now());
});

function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}
