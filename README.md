# Robert AI

## Starting with forever and logging

### Install forever

sudo npm install forever -g

### Start and log

forever start -o out.log -e err.log -a robert-slack.js

