require('dotenv').load();

var capitalize = require('capitalize');

var unirest = require('unirest');
var axios = require('axios');

var mkdirp = require('mkdirp');

var shell = require('shelljs');

var request = require('request');
var fs = require("fs");
var crypto = require("crypto");
var moment = require('moment');
var Botkit = require('botkit');
var express = require('express');

var reampLoginToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzE0fQ.ZLjbhqtO1ZwmA7GH2MUeRAI1vp6Km1p3D3xEMNh4MYg';

var _ = require('underscore');

var Sequelize = require('sequelize');

var sequelize = new Sequelize(process.env.MYSQL_DB, process.env.MYSQL_USER, process.env.MYSQL_PASS, {
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    pool: {
        max: 500,
        min: 10,
        idle: 5000,
        acquire: 5000,
        evict: 10000,
        handleDisconnects: true
    },
    retry:{max: 3},
    logging: false,
    dialectOptions: {
                requestTimeout: 0,
    }
});

var robertTasks = sequelize.define('robert_tasks', {
    username: { type: Sequelize.STRING, allowNull: true },
    userid: Sequelize.STRING,
    message: { type: Sequelize.STRING, allowNull: true },
    jobstatus: { type: Sequelize.STRING, allowNull: true }, // pending, answered, completed
    jobname: { type: Sequelize.STRING, allowNull: true },
    intent: { type: Sequelize.STRING, allowNull: true },
    params: { type: Sequelize.TEXT, allowNull: true }, // json with all job params from watson
});

var people = sequelize.define('reamp_people', {
    username: { type: Sequelize.STRING, allowNull: true },
    userid: Sequelize.STRING,
    rmcId: {type: Sequelize.STRING, allowNull: true},
    real_name: { type: Sequelize.STRING, allowNull: true },
    nick_name: { type: Sequelize.STRING, allowNull: true },
    email: { type: Sequelize.STRING, allowNull: true },
    area: { type: Sequelize.STRING, allowNull: true }, // opec, midia e audiencia, tecnologia, bi, administrativo
    cargo: { type: Sequelize.STRING, allowNull: true },
    ramal: { type: Sequelize.STRING, allowNull: true },
    status_cadastro: { type: Sequelize.STRING, allowNull: true }, // esperando cadastro, cadastrada
    params: { type: Sequelize.TEXT, allowNull: true }
});