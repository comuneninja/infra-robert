var settings = require("./settings.js");
var CalendarAPI = require('node-google-calendar');
var cal = new CalendarAPI(settings);
var moment = require('moment');

sala6 = settings.calendarSala6;
sala5 = settings.calendarSala5;

module.exports = {
    listEvents: function (start, end) {
        cal.Events.list(calendarId, params)
            .then(json => {
                //Success
                console.log('List of events on calendar within time-range:');
                console.log(json);
            }).catch(err => {
                //Error
                console.log('Error: listSingleEvents -' + err.message);
            });
    },

    consularSalas: function (start, end) {
        // ver sala 5

        // ver sala 6

        // se alguma estiver disponivel, responder que esta disponivel
    },

    consultarSala5: function (start, end) {

    },

    consultarSala6: function (start, end) {

    },

    insertEvent: function (dateStringStart, dateStringEnd, organizer, nome_evento, atendees, salaStr) {
        var start = moment(dateStringStart, "YYYY-MM-DD HH:mm").format("YYYY-MM-DDTHH:mm:ss-03:00");
        var end = moment(dateStringEnd, "YYYY-MM-DD HH:mm").format("YYYY-MM-DDTHH:mm:ss-03:00");

        let event = {
            'start': { 'dateTime': start },
            'end': { 'dateTime': end },
            'location': salaStr,
            'summary': nome_evento,
            'status': 'confirmed',
            'description': ''
        };


        var sala = sala5;
        if (salaStr == 'sala6') sala = sala6;
        var ins = cal.Events.insert(sala, event)
            .then(resp => {
                return resp;
            })
            .catch(err => {
                return err;
            });

        return Promise.resolve(ins);
    },

    teste: function () {
        console.log("queijo");
    },

    checkFreeOrBusy: function (dateStringStart, dateStringEnd, salaStr) {
        // tratar data como string mesmo, vem no formato yyyy-MM-dd

        var start = moment(dateStringStart, "YYYY-MM-DD HH:mm").format("YYYY-MM-DDTHH:mm:ss-03:00");
        var end = moment(dateStringEnd, "YYYY-MM-DD HH:mm").format("YYYY-MM-DDTHH:mm:ss-03:00");

        var sala = sala5;
        if (salaStr == 'sala6') sala = sala6;

        let params = {
            "timeMin": start,
            "timeMax": end,
            "items": [{ "id": sala }]
        };


        var q = cal.FreeBusy.query(sala, params)
            .then(resp => {
                console.log(resp);
                //resolve(resp);
                return resp;
            })
            .catch(err => {
                //reject(err);
                return err;
            });

        return Promise.resolve(q);
    }
}
