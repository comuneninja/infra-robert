/**
 * Copyright 2016 IBM Corp. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require('dotenv').load();

var capitalize = require('capitalize');

var unirest = require('unirest');
var axios = require('axios');

var mkdirp = require('mkdirp');

var shell = require('shelljs');

var request = require('request');
var fs = require("fs");
var crypto = require("crypto");
var moment = require('moment');
var Botkit = require('botkit');

var reampLoginToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzE0fQ.ZLjbhqtO1ZwmA7GH2MUeRAI1vp6Km1p3D3xEMNh4MYg';

var _ = require('underscore');

var Sequelize = require('sequelize');

var sequelize = new Sequelize(process.env.MYSQL_DB, process.env.MYSQL_USER, process.env.MYSQL_PASS, {
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    pool: {
        max: 500,
        min: 10,
        idle: 5000,
        acquire: 5000,
        evict: 10000,
        handleDisconnects: true
    },
    retry:{max: 3},
    logging: false,
    dialectOptions: {
                requestTimeout: 0,
    }
});

var robertTasks = sequelize.define('robert_tasks', {
    username: { type: Sequelize.STRING, allowNull: true },
    userid: Sequelize.STRING,
    message: { type: Sequelize.STRING, allowNull: true },
    jobstatus: { type: Sequelize.STRING, allowNull: true }, // pending, answered, completed
    jobname: { type: Sequelize.STRING, allowNull: true },
    intent: { type: Sequelize.STRING, allowNull: true },
    params: { type: Sequelize.TEXT, allowNull: true }, // json with all job params from watson
});

var people = sequelize.define('reamp_people', {
    username: { type: Sequelize.STRING, allowNull: true },
    userid: Sequelize.STRING,
    rmcId: {type: Sequelize.STRING, allowNull: true},
    real_name: { type: Sequelize.STRING, allowNull: true },
    nick_name: { type: Sequelize.STRING, allowNull: true },
    email: { type: Sequelize.STRING, allowNull: true },
    area: { type: Sequelize.STRING, allowNull: true }, // opec, midia e audiencia, tecnologia, bi, administrativo
    cargo: { type: Sequelize.STRING, allowNull: true },
    ramal: { type: Sequelize.STRING, allowNull: true },
    status_cadastro: { type: Sequelize.STRING, allowNull: true }, // esperando cadastro, cadastrada
    params: { type: Sequelize.TEXT, allowNull: true }
});

robertTasks.findAll({ where: {jobstatus: "requested"} }).then(function (resultSet, bot) {
    console.log(resultSet);

    resultSet.forEach(function (one) {
        var intent = one.intent;
        var id = one.params;

        if(intent === "invoice") {
            var params = {
                "invoice": {
                    "id": one.params
                }
            };
            
            axios.get('http://nemesis.reamp.com.br/v1/invoices/' + id, { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } })
            .then(function (response) {
                var responseBody = response.data;
                if (responseBody.data.attributes.file) {
                    var name = responseBody.data.attributes.name;

                    console.log(responseBody.data.attributes.file)

                    one.updateAttributes({
                        jobstatus: 'done',
                        message: 'Seu invoice ' + name + ' está em ' + responseBody.data.attributes.file
                    })
                    .success(function () {});
                }
            }.catch(error => {
                console.error(error.response.data.message);
                bot.reply(message, "Ocorreu um erro: " + error.response.data.message);
            }));
        }
        
    });
});

process.exit(1);