var axios = require('axios');
var moment = require('moment');
var rjobvariables = require('./reamp-job-variables.js');

module.exports = {
  execute: function(bot, message, startDate, endDate, userId) {
  	debugger;
          var reampLoginToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzEyfQ.S5zBc662Hrw2ik6149SnGcd7tCYgS8ElLDAZDzeiqUk';
          var overlapCreatedAt = moment(new Date()).format("YYYY-MM-DD");
          var overlapUpdatedAt = moment(new Date()).format("YYYY-MM-DD");

          // var accountWOSpecial = (message.watsonData.context.conta.replace("&amp;", "")).replace(/[^a-zA-Z]\s+/ig, "").toUpperCase();
          // var accountWOSpace = (message.watsonData.context.conta.replace("&amp;", " ")).replace(/\s+/ig," ");

          axios.get('http://nemesis.reamp.com.br/v1/accounts', { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } })
            .then(function (response) {
              var arr = []
              response.data.data.map((item) => {
                arr.push({
                  "accountName": (item.attributes.name.replace("&", "")).replace(/[^a-zA-Z]\s+/ig, "").toUpperCase(),
                  "accountId": item.id
                })
              });

              var account = arr.filter(function(acc) {
                var accountWOSpecial = (message.watsonData.context.conta.replace("&amp;", "")).replace(/[^a-zA-Z]\s+/ig, "").toUpperCase();
                return acc.accountName == accountWOSpecial;
              });

              var name = "inv" + overlapCreatedAt;

              axios.get('http://nemesis.reamp.com.br/v1/accounts/name/' + account[0].accountName, { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } }).then(response => {
                var responseBody = response.data;
                if(responseBody){
                    axios.get('http://nemesis.reamp.com.br/v1/accounts/' + account[0].accountId,
                        { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } }).then(response => {
                          var responseBody = response.data;

                          if (responseBody) {

                            var params = {
                              "report_job": {
                                "account": arr.accountId,
                                "start_date": startDate,
                                "end_date": endDate,
                                "report_type": "report-overlap-campaign",
                                "file_name": "...",
                                "campaigns": message.watsonData.context.campanhas,
                                "status": "requested",
                                "created_at": overlapCreatedAt,
                                "updated_at": overlapUpdatedAt,
                                "user": userId
                              }
                            };

                            //POST
                            axios.post('http://nemesis.reamp.com.br/v1/report_jobs', params,
                              { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } }).then(response => {
                                var responseBody = response.data;
                                var id = responseBody.data[0].id;

                                bot.reply(message, "Entrarei em contato quando o relatório de overlap estiver pronto :)");
                                rjobvariables.execute(message);
                              }).catch(error => {
                                bot.reply(message, "Desculpa, mas não consegui tirar o seu relatório de overlap. Avisa pro TI que deu esse erro aqui: " + error.response.data.message);
                                rjobvariables.execute(message);
                                console.error(error.response.data.message);
                              });
                            } 
                        }).catch(error => {
                          rjobvariables.execute(message);
                          console.error(error.response.data.message);
                        });
                }
              }).catch(error => {
                bot.reply(message, "Não encontrei essa conta :/");
                rjobvariables.execute(message);
                console.error(error.response.data.message);
              });
            })
            .catch(function (error) {
              console.log(error);
            });
        }

  // }
}