var moment = require('moment');
var axios = require('axios');

module.exports = {
	execute: function(bot, message) {
		console.log("criando billing data");

		var reampLoginToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzEyfQ.S5zBc662Hrw2ik6149SnGcd7tCYgS8ElLDAZDzeiqUk';
		var params = 
		{
		  "billing_data": {
		    "account": "2000002",
		    "company_name": "Reamp",
		    "cnpj": 123456789,
		    "responsible": "Emmanuel",
		    "agency": "Reamp",
		    "email": "johndoe@gmail.com",
		    "phone": 11912349876,
		    "country": "Brasil",
		    "state": "SP",
		    "city": "Sao Paulo",
		    "neighborhood": "Neighborhood 12-34",
		    "cep": 12345999,
		    "address": "rua agusta, 1642",
		    "created_at": "2017-05-08 10:00",
		    "updated_at": "2017-05-08 10:00",
		    "user": "1312"
		  }
		}

		axios.post('http://admin-adserver-api.reamp.com.br/v1/billing_datas', params,
		   { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } }).then(response => {

		   	var responseBody = response.data;

		   	if (responseBody) {
		   		console.log(responseBody);
		   	}
		}).catch(error => {
		   	console.error(error.response.data.message);
		});
	}
}
