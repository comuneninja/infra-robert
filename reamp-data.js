module.exports = {
    
    endOfDay: function(startDate) {
        var endDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 23, 59, 59);
        return endDate;
    },

    randomName: function(size) {
        return crypto.randomBytes(size).toString('hex');
    },

    horaCheia: function(hora) {
        var re = new RegExp('([0-9]+):');
        return hora.match(re)[1];
    }
}