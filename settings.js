var fs = require('fs');
const KEYPATH = 'Robert-89e747df912d.json';
var json = fs.readFileSync(KEYPATH, 'utf8');
var key = JSON.parse(json).private_key;

const SERVICE_ACCT_ID = 'robertbot@robert-181918.iam.gserviceaccount.com';

const CALENDAR_URL = 'https://calendar.google.com/calendar/ical/reamp.com.br_k84jni2cr0qealr3sf7ps2fr50%40group.calendar.google.com/private-ff0c0380a2f21a96d40a05cd9a0f2ced/basic.ics';
const CALENDAR_ID = {
  'primary': 'reamp.com.br_ad1gcs5a3o355mujhfpje6rfsg@group.calendar.google.com',
  'calendar-1': 'reamp.com.br_p4d4brbj9a32ta038u6q7lt38o@group.calendar.google.com'
};

const TIMEZONE = 'UTC-03:00';

module.exports.calendarUrl = CALENDAR_URL;
module.exports.serviceAcctId = SERVICE_ACCT_ID;
module.exports.calendarId = CALENDAR_ID;

//module.exports.keyFile = KEYFILE;           //or if using json keys - module.exports.key = key; 
module.exports.key = key;

module.exports.calendarSala6 = 'reamp.com.br_ad1gcs5a3o355mujhfpje6rfsg@group.calendar.google.com';
module.exports.calendarSala5 = 'reamp.com.br_p4d4brbj9a32ta038u6q7lt38o@group.calendar.google.com';

module.exports.timezone = TIMEZONE;