var rcalendar = require('./reamp-calendar.js');
var rjobvariables = require('./reamp-job-variables.js');

module.exports = {
    execute: function (bot, message) {
        var dataAgendamento = message.watsonData.context.dia;

        var horaInicio = message.watsonData.context.horaInicio;
        var horaFim = message.watsonData.context.horaFim;

        console.log("data agendamento: " + dataAgendamento);
        console.log("hora inicio: " + horaInicio);
        console.log("hora fim: " + horaFim);

        var start = dataAgendamento + " " + horaInicio;
        var end = dataAgendamento + " " + horaFim;

        rcalendar.checkFreeOrBusy(start, end, 'sala5').then(resp => {
            var busySala5 = 0;
            busySala5 += resp ? resp.length : 0;
            if (busySala5 == 0) {
                rcalendar.insertEvent(start, end, 'Robert.Bot', message.watsonData.context.nomeEvento, 'it@reamp.com.br', 'sala5').then(resp => {
                    console.log(resp);
                    if(resp){
                        bot.reply(message, message.watsonData.context.nomeEvento + " registrado na sala 5");
                        rjobvariables.execute(message);
                    }    
                }).catch(error => {
                bot.reply(message, error);
                rjobvariables.execute(message);
            });
            } else {
                rcalendar.checkFreeOrBusy(start, end, 'sala6').then(resp2 => {
                    var busySala6 = 0;
                    if(resp2){
                        busySala6 += resp2 ? resp2.length : 0;
                        if (busySala6 == 0) {
                            rcalendar.insertEvent(start, end, 'Robert.Bot', message.watsonData.context.nomeEvento, 'it@reamp.com.br', 'sala6').then(resp => {
                                console.log(resp);
                                if(resp){
                                    bot.reply(message, message.watsonData.context.nomeEvento + " registrado na sala 6");
                                    rjobvariables.execute();
                                }
                            }).catch(error => {
                            bot.reply(message, error);
                            rjobvariables.execute(message);
                        });
                        } else {
                            bot.reply(message, "Esse horário está ocupado nas duas salas");
                            rjobvariables.execute(message);
                        }
                    }
                }).catch(error => {
                bot.reply(message, error);
                rjobvariables.execute(message);
            });
            }
        });
    }
}